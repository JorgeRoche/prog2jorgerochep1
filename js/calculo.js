function operacion() {
    
    var calculo = document.getElementById("monto").value;
    var text;
    //si la comision esta entre 0 - 999Bs Comision=5%, 1.000 - 10.000 Bs. la Comision=10%, mayor a 10.000 15%
    
    if (calculo >= 0 && calculo <= 999) {
        
        var comision = (calculo * (5/100)).toFixed(2);
        alert ("La Comision sera del 5%");
        text = comision;
        
    }
    
    else if (calculo >= 1000 && calculo <= 10000) {
        
        var comision = (calculo * (10/100)).toFixed(2);
        alert ("La Comision sera del 10%");
        text = comision;
        
    }
    
    else if (calculo > 10000) {
        
        var comision = (calculo * (15/100)).toFixed(2);
        alert ("La Comision sera del 15%");
        text = comision;
        
    }
    
    else if (calculo < 0) {
        text = "Ingrese un Valor Valido";
        
    }
    
    document.getElementById("resultado").innerHTML = text;
    
}